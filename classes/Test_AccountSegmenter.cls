/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AccountSegmenter {

    static testMethod void myUnitTest() { 
    	
        // TO DO: implement unit test
        test.startTest();

        List<Account> accs = UTestData.getAccounts(3);
        insert accs;
        List<Product2> prods = new List<Product2>();//UTestData.getProducts(3);
        
        /*Uncommented by Trekbin based on Case  00025200 on 15 Feb, 2016*/
        List<Sub_Cat_Final__c> subcats = UTestData.getSubCats(1); 
        insert subcats;
        
        for(Product2 prod : UTestData.getProducts(3))
        {
        	/*Uncommented and modified by Trekbin based on Case  00025200 on 15 Feb, 2016*/
        	prod.Sub_Cat_Final__c = subcats[0].id;
        	prods.add(prod);
        }
        insert prods;
        List<String> skus = new List<String>{'TEST'};
        
        List<Asset> assets = UTestData.getAssetsWithProductAndSubCat(accs, prods);
        insert assets;

        List<Invoice__c> invoices = UTestData.getInvoices(accs, -30);
        insert invoices;
        
        List<Invoice_Detail__c> invoicedetails = UTestData.getInvoiceDetails(invoices, prods.size());
        //**loop through invoice details and assign product/sku/subcat?
        insert invoicedetails;
        
        
        //build account segmenter with product, subcategory, and sku filters
        List<Account_Segmenter__c> accSegs = UTestData.getSegmenters(2);
        insert accSegs;
        List<Filter_Group__c> filtergroups = UTestData.getFilterGroups(accSegs[0], 2);
        insert filtergroups;
        List<Filter_Group__c> filtergroups2 = UTestData.getFilterGroups(accSegs[1],2);
        insert filtergroups2;
        //new filter group for:
        //search by asset past two years, go ahead and set product, subcat, sku status and condition.
        //search by invoice past two years, go ahead and set........................................
		//
		List<Filter_Group__c> fgtoupdate = new List<Filter_Group__c>();
		List<Filter_Group_Item__c> fgitoinsert = new List<Filter_Group_Item__c>();
		Integer counter1 = 0;
		for(filter_group__c fg : filtergroups)
		{	//AND EXISTS, DNE
			fg.Product_Condition__c = 'AND';
			fg.Subcategory_Condition__c = 'AND';
			fg.SKU_Condition__c = 'AND';
			if(counter1==0)
			{
				fg.Product_Status__c = 'EXISTS';	
			}else
			{
				fg.Product_Status__c = 'DOES NOT EXIST';
			}
			if(counter1==0)
			{
				fg.Subcategory_Status__c = 'EXISTS';	
			}else
			{
				fg.Subcategory_Status__c = 'DOES NOT EXIST';
			}
			if(counter1==0)
			{
				fg.SKU_Status__c = 'EXISTS';	
			}else
			{
				fg.SKU_Status__c = 'DOES NOT EXIST';
			}
			if(counter1==0)
			{
				fg.Search_By__c = 'Assets';
			}else
			{
				fg.Search_By__c = 'Invoices';
			}
			fg.Start_Date__c = system.now().date().addDays(-365);
			fg.End_Date__c = system.now().date().addDays(365);
			
			fgtoupdate.add(fg);
			
			/*Uncommented by Trekbin based on Case  00025200 on 15 Feb, 2016*/
			fgitoinsert.addall(UTestData.getFilterGroupItems(fg,prods,subcats,skus)); 
			//fgitoinsert.addall(UTestData.getFilterGroupItems(fg,prods,skus));
			counter1++;
		}
		Integer counter2 = 0;
		for(filter_group__c fg : filtergroups2)
		{
			//Or EXISTS, DNE
			fg.Product_Condition__c = 'OR';
			fg.Subcategory_Condition__c = 'OR';
			fg.SKU_Condition__c = 'OR';
			if(counter2==0)
			{
				fg.Product_Status__c = 'EXISTS';	
			}else
			{
				fg.Product_Status__c = 'DOES NOT EXIST';
			}
			if(counter2==0)
			{
				fg.Subcategory_Status__c = 'EXISTS';	
			}else
			{
				fg.Subcategory_Status__c = 'DOES NOT EXIST';
			}
			if(counter2==0)
			{
				fg.SKU_Status__c = 'EXISTS';	
			}else
			{
				fg.SKU_Status__c = 'DOES NOT EXIST';
			}
			if(counter2==0)
			{
				fg.Search_By__c = 'Assets';
			}else
			{
				fg.Search_By__c = 'Invoices';
			}
			fg.Start_Date__c = system.now().date().addDays(-365);
			fg.End_Date__c = system.now().date().addDays(365);
			
			fgtoupdate.add(fg);
			
			/*Uncommented by Trekbin based on Case  00025200 on 15 Feb, 2016*/
			fgitoinsert.addall(UTestData.getFilterGroupItems(fg,prods,subcats,skus));
			//fgitoinsert.addall(UTestData.getFilterGroupItems(fg,prods,skus));
			counter2++;
		}
		update fgtoupdate;
		insert fgitoinsert;
		
		system.debug('-- fgs: ' + fgtoupdate);
		system.debug('-- fgis: ' + fgitoinsert);
		
        for(Account_Segmenter__c accsg : accSegs)
        {
        	PageReference pageref = page.AccountSegmenter;
        	apexpages.Standardcontroller sc = new apexpages.Standardcontroller(accsg);
        	uaccountsegmentercontroller uas = new uaccountsegmentercontroller(sc);
        	
        	test.setCurrentPage(pageref);

        	boolean ps = uas.pageStep; 
        	List<string> acv = uas.accountClassValue;
        	List<selectoption> aco = uas.accountClassOptions;
        	List<string> aci = uas.accountIndustryValue;
        	List<selectoption> acio = uas.accountIndustryOptions;
        	List<UAccountSegmenterController.FilterGroupWrapper> fgs = uas.getfiltergroups();
        	boolean fgshow = fgs[0].showGroup;
        	//fgs[0].addProduct();
        	fgs[0].addSKU();
        	fgs[0].addSubcategory();
        	fgs[0].removeSelected();
        	
        	uas.addGroup();
        	uas.saveOverride();
        	
        	uas.searchAccounts();

			List<UAccountSegmenterController.AccountWrapper> aws = uas.accounts;
			system.debug('-- account size: ' + aws.size());


			uas.allSelected = true;
			uas.selectAll();
			uas.nextpage();
			uas.lastpage();
			uas.previouspage();
			Integer pn = uas.pageNumber;
			Integer ttlpges = uas.totalPages;
			boolean hn = uas.hasNext;
			boolean hp = uas.hasPrevious;
			uas.firstpage();


			//generate action
			
			uas.generateAction();
			uas.actionDate = system.now().date().adddays(30);
			uas.actionSubject = 'test';
			uas.actionTag = 'test';
			
			uas.submitAction();
			/* Last Modified by Trekbin on 18 March, 2016 as per case: 00025200 */
			uas.exportAccountsAsCSV();
			uas.exportAccountsAsCSV();
			uas.cancel();
			uas.cancelAction();
			uas.goHome();
			fgs[0].removeGroup();

        }
        
        

        test.stopTest();  
		
		//assert selected map > 0 and query back to to make sure tasks exist
        system.assert([Select Id from Task].size()>0);   
          
    }
}