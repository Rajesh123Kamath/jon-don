/**
    @ClassName      : UCustomPopupUserHndlr
    @CreatedBy      : Trekbin
    @CreatedOn      : 03-March-2016
    @ModifiedBy     : Trekbin on 18 March, 2016 as per case: 00025200
    @Description    : Handler for popup page CustomPopupUserPage
*/
public with sharing class UCustomPopupUserHndlr
{
    
	public List<User> lstUserResults {get;set;} // search results
	public string searchString {get;set;} // search keyword
	
	public UCustomPopupUserHndlr() {
		
		//objProject = new Project__c();
		
		// get the current search string
		searchString = System.currentPageReference().getParameters().get('lksrch');
		runSearch();  
	}
	
	// performs the keyword search
	public PageReference search() {
		
		runSearch();
		return null;
	}
	
	// prepare the query and issue the search command
	private void runSearch() {
		// TODO prepare query string for complex serarches & prevent injections
		lstUserResults = performSearch(searchString);               
	} 
	
	/* Last Modified by Trekbin on 18-march, 2016 for displaying only active users */
	// run the search and return the records found. 
	private List<User> performSearch(string searchString) {
		
		String soql;
		
		soql = 'Select Id, Name From User Where IsActive = true';
		
		// get requested user's role
		Id roleId = [Select UserRoleId from User where Id = :userInfo.getUserId()].UserRoleId;
		
		if(searchString != '' && searchString != null)
			soql +=  ' AND name LIKE \'%' + searchString +'%\'';
				
		if(roleId != null)
		{
			// get all of the roles underneath the user
			Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
			soql += ' AND UserRoleId IN : allSubRoleIds';
		}
			
		soql = soql + ' limit 25';
		return database.query(soql); 
	}
	
  	private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

		Set<ID> currentRoleIds = new Set<ID>();
	
	    // get all of the roles underneath the passed roles
	    for(UserRole userRole : [Select Id from UserRole where ParentRoleId IN :roleIds AND ParentRoleID != null])
	    	currentRoleIds.add(userRole.Id);
	
	    // go fetch some more rolls!
	    if(!currentRoleIds.isEmpty())
	    	currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
	
	    return currentRoleIds;
  	}
	
	// used by the visualforce page to send the link to the right dom element
	public string getFormTag() {
		return System.currentPageReference().getParameters().get('frm');
	}
	
	// used by the visualforce page to send the link to the right dom element for the text box
	public string getTextBox() {
		return System.currentPageReference().getParameters().get('txt');
	}
}