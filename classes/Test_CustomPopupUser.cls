/**
    @TestClassName	: Test_CustomPopupUser
    @HandlerName    : UCustomPopupUserHndlr
    @CreatedBy      : Trekbin
    @CreatedOn      : 03-March-2016
    @ModifiedBy     : 
    @Description    : Test class for the Handler "UCustomPopupUserHndlr" for popup page CustomPopupUserPage
*/
@isTest(SeeAllData = false)
private class Test_CustomPopupUser {

    /**
    	Test Method: customPopupTest
    */
    static testMethod void customPopupTest() {

		UserRole testAndyRole = [Select PortalType, Name, Id, DeveloperName From UserRole Where PortalType = 'None' Limit 1];
		
		
		//Fetch profiles
      	List<String> ps = new List<String>();
      	
		ps.add(UTestData.getStandardProfileId('Standard User'));
		ps.add(UTestData.getStandardProfileId('System Administrator'));
		
				
		//List of test users to insert
		List<User> lstTestUsersToInsert = new List<User>();
		
		lstTestUsersToInsert.add(UTestData.getUserRecord(ps[0]));
		lstTestUsersToInsert.add(UTestData.getUserRecord(ps[1]));
		
		//Test user with ANDY Role
		lstTestUsersToInsert[0].UserRoleId = testAndyRole.Id;
		
		
		//No role for 3rd user
		
		//Insert the users
		insert lstTestUsersToInsert;
		
		//Start a test
		Test.startTest();
			
			
			//Run as andy roles user
			System.runAs(lstTestUsersToInsert[0]) {
				
				system.currentPageReference().getParameters().put('lksrch', 'Testinga2');
				
				//Instantiate a class
				UCustomPopupUserHndlr objUCustomPopup = new UCustomPopupUserHndlr();
				objUCustomPopup.search();
				
	    	}
	    		    		    	
	    	//Run as andy roles user
			System.runAs(lstTestUsersToInsert[1]) {
				
				system.currentPageReference().getParameters().put('lksrch', 'Testinga1');
				
				//Instantiate a class
				UCustomPopupUserHndlr objUCustomPopup = new UCustomPopupUserHndlr();
				objUCustomPopup.search();
					
	    	}
    	//Stop a test
		Test.stopTest();
		
    }
}